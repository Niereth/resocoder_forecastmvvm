package com.resocoder.forecastmvvm.data.network

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.resocoder.forecastmvvm.data.network.response.CurrentWeatherResponse
import com.resocoder.forecastmvvm.internal.NoConnectivityException

class WeatherNetworkDataSourceImpl(
    private val weatherstackApiService: WeatherstackApiService
) : WeatherNetworkDataSource {

    private val _downloadedCurrentWeather = MutableLiveData<CurrentWeatherResponse>()

    override
    val downloadedCurrentWeather: MutableLiveData<CurrentWeatherResponse> = MutableLiveData()

    override suspend fun fetchCurrentWeather(location: String, unitsCode: String) {
        try {
            val fetchedCurrentWeather = weatherstackApiService
                .getCurrentWeather(location, unitsCode)
                .await()
            downloadedCurrentWeather.postValue(fetchedCurrentWeather)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet connection.", e)
        }
    }
}