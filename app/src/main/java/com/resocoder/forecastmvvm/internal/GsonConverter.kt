package com.resocoder.forecastmvvm.internal

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.Collections.emptyList

class GsonConverter {

    private val gson = Gson()

    @TypeConverter
    fun toJson(list: List<String>): String {
        return gson.toJson(list)
    }

    @TypeConverter
    fun fromJson(value: String?): List<String> {
        if (value == null) {
            return emptyList()
        }

        val listType = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(value, listType)
    }
}