package com.resocoder.forecastmvvm.internal

enum class UnitSystem(val value: String) {
    METRIC("m"),
    IMPERIAL("f")
}